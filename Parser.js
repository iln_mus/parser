class Parser {

    constructor(params) {
        this._initial_exclude = ['Какой-то']
        this.p = params ? params : {}
        // Минимальная длина слова
        this.word_length = this.get_correct_number(this.p.word_length, 1)
        // Кол-во символов от начала текста
        this.symbols_count = this.get_correct_number(this.p.symbols_count, 0)
        // Список слов, которые исключаем
        this.exclude_words = this.set_exclude_words(this.p.exclude_words, this.p.is_initial_exclude)
        // регулярка
        this.regexp = this.p.regexp ? this.p.regexp : false
    }

    parse(text, callback) {
        try {
            if(!text || text === null || text.length <= 0) {
                throw new Error('Введите текст!')
            }
            //Делаем все в lowerCase
            text = text.toLowerCase()
            //Убираем html тэги
            text = this.strip_html_tags(text)
            // Обрезаем строку до заданного кол-ва символов
            text = this.symbols_count > 0 ? text.substring(0, this.symbols_count) : text
            // Делим строку по словам
            let parsed_words =
            text.replace(/\’/g, ' ').replace(/[^\w\s\А-Я\а-я]/gi, '').split(' ')
            // выпиливаем слова, которые исключены, проверяем регуляркой
            // и проверяем на минимальную длину слова
            parsed_words = parsed_words.filter((el) => {
                return (
                    this.exclude_words.indexOf( el ) < 0 &&
                    (this.regexp ? this.regexp.test(el) : true) &&
                    this.word_length <= el.length
                )
            })
            // Подсчитываем кол-во ключеых слов
            let result = this.get_count_of_words(parsed_words)
            callback(null, result)
        } catch(err) {
            callback(err, null)
        }
    }

    // проверяет на наличие и положительность val,
    // еслb нет val, возвращаем initial (значение по умолчанию)
    get_correct_number(val, initial = 0) {
        if(!val || val === null) {
            return initial
        }
        if(val < 0) {
             throw new Error('Введите положительное число')
        }
        if(val >= 0) {
            return val
        }
    }

    set_exclude_words(exclude_words, is_initial_exclude) {
        // Если такого параметра нет
        if(!exclude_words || exclude_words === null) {
            // если нужно учитывать нач. искл.
            if(is_initial_exclude) {
                // возвращаем начальные знач.
                return this._initial_exclude
            } else {
                // иначе пустой массив
                return []
            }
        } else {
            if(!Array.isArray(exclude_words)) {
                // Если не массив, бросаем исключение
                 throw new Error('Задайте список исключаемых слов в виде массива')
            }
            // Соединяем два массива, если нужно учитывать нач. искл.
            let res = is_initial_exclude ?
                exclude_words.concat(this._initial_exclude) :
                exclude_words
            // унифицируем, переводим в нижний регистр
            return this.unique(res.join().toLowerCase().split(','))
        }
    }

    unique(array) {
        return (
            array.filter((v, i, a) => a.indexOf(v) === i)
        )
    }

    strip_html_tags(str) {
        if (str===null || str==='') {
            return false
        } else {
            str = str.toString();
            return str.replace(/<[^>]*>/g, ' ');
        }
    }

    get_count_of_words(array) {
        return (
            array.reduce((acc, el) => {
                acc[el] = (acc[el] || 0) + 1;
                return acc;
              }, {})
        )
    }
}


let parser = new Parser({
    is_initial_exclude: true,
    word_length: 2,
    // regexp: /[А-Яа-я]/g,
    // symbols_count: 100,
    exclude_words: [
        'if',
        'in',
        'at',
        'be',
        'll'
    ]
})

parser.parse(
    'About John… If <a href="#">блфблф</a> John goes<div>asdasd</div>home in monday at 10pm, I’ll be happy!',
    (err, res) => {
        console.log(err)
        console.log(res)
    });